#ifndef BINARY_SEARCH_TREE_HPP
#define BINARY_SEARCH_TREE_HPP

#include <memory>
#include <ostream>

class BinarySearchTree {
  protected:
    class Node {
      public:
        std::unique_ptr<Node> left_child  = nullptr;
        std::unique_ptr<Node> right_child = nullptr;
      private:
        double element                    = 0;
        unsigned height                   = 1;

      public:
        Node(double element);

        double get_element() const;

        unsigned get_height() const;
        void update_height();

        const Node& get_left_child() const;
        const Node& get_right_child() const;
        Node& get_left_child();
        Node& get_right_child();

        void create_left_child(double element);
        void create_right_child(double element);

        bool has_left_child() const;
        bool has_right_child() const;

        friend std::ostream& operator<<(std::ostream& os, const Node& node);
    };

  protected:
    std::unique_ptr<Node> root = nullptr;

  public:
    virtual void insert(double element);
    unsigned get_height() const;
    void inorder(std::ostream& os) const;
    void preorder(std::ostream& os) const;
    friend std::ostream& operator<<(std::ostream& os, const Node& node);

  private:
    static void inorder_to_stream(std::ostream& os, const Node& node);
    static void preorder_to_stream(std::ostream& os, const Node& node);
    static void insert_non_empty(BinarySearchTree::Node& node, double element);
};

#endif
