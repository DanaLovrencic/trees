#ifndef AVL_TREE_HPP
#define AVL_TREE_HPP

#include "BinarySearchTree.hpp"

#include <vector>
#include <optional>

class AVLTree : public BinarySearchTree {
  public:
    void insert(double element) override;
    void print();

  private:
    static void insert_non_empty(std::unique_ptr<BinarySearchTree::Node>& node,
                                 double element);
    static void rotate_right(std::unique_ptr<BinarySearchTree::Node>& y);
    static void rotate_left(std::unique_ptr<BinarySearchTree::Node>& x);
    static int get_balance_factor(const BinarySearchTree::Node& node);
    using ElementsByLevel = std::vector<std::vector<std::optional<double>>>;
    static void set_inorder_elements(ElementsByLevel& elements,
                                     const BinarySearchTree::Node& node,
                                     unsigned height);
};

#endif
