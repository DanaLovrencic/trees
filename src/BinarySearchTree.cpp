#include "BinarySearchTree.hpp"

#include <stdexcept>

BinarySearchTree::Node::Node(double element) : element(element) {}

double BinarySearchTree::Node::get_element() const
{
    return element;
}

unsigned BinarySearchTree::Node::get_height() const
{
    return height;
}

void BinarySearchTree::Node::update_height()
{
    unsigned left_height  = left_child ? left_child->get_height() : 0;
    unsigned right_height = right_child ? right_child->get_height() : 0;
    height                = 1 + std::max(left_height, right_height);
}

const BinarySearchTree::Node& BinarySearchTree::Node::get_left_child() const
{
    if (!has_left_child()) throw std::logic_error("Node has no left child!");
    return *left_child;
}

const BinarySearchTree::Node& BinarySearchTree::Node::get_right_child() const
{
    if (!has_right_child()) throw std::logic_error("Node has no right child!");
    return *right_child;
}

BinarySearchTree::Node& BinarySearchTree::Node::get_left_child()
{
    if (!has_left_child()) throw std::logic_error("Node has no left child!");
    return *left_child;
}

BinarySearchTree::Node& BinarySearchTree::Node::get_right_child()
{
    if (!has_right_child()) throw std::logic_error("Node has no right child!");
    return *right_child;
}

void BinarySearchTree::Node::create_left_child(double element)
{
    if (has_left_child()) throw std::logic_error("Node has left child!");
    left_child = std::make_unique<BinarySearchTree::Node>(element);
}

void BinarySearchTree::Node::create_right_child(double element)
{
    if (has_right_child()) throw std::logic_error("Node has right child!");
    right_child = std::make_unique<BinarySearchTree::Node>(element);
}

bool BinarySearchTree::Node::has_left_child() const
{
    return left_child != nullptr;
}

bool BinarySearchTree::Node::has_right_child() const
{
    return right_child != nullptr;
}

void BinarySearchTree::insert(double element)
{
    if (!root)
        root = std::make_unique<Node>(element);
    else
        insert_non_empty(*root, element);
}

unsigned BinarySearchTree::get_height() const
{
    return root ? root->get_height() : 0;
}

void BinarySearchTree::inorder(std::ostream& os) const
{
    if (root) {
        BinarySearchTree::inorder_to_stream(os, *root);
    } else {
        os << "Empty tree!";
    }
}

void BinarySearchTree::preorder(std::ostream& os) const
{
    if (root) {
        BinarySearchTree::preorder_to_stream(os, *root);
    } else {
        os << "Empty tree!";
    }
}

void BinarySearchTree::inorder_to_stream(std::ostream& os, const Node& node)
{
    if (node.has_left_child()) inorder_to_stream(os, node.get_left_child());
    os << node << " ";
    if (node.has_right_child()) inorder_to_stream(os, node.get_right_child());
}

void BinarySearchTree::preorder_to_stream(std::ostream& os, const Node& node)
{
    os << node << " ";
    if (node.has_left_child()) preorder_to_stream(os, node.get_left_child());
    if (node.has_right_child()) preorder_to_stream(os, node.get_right_child());
}

std::ostream& operator<<(std::ostream& os, const BinarySearchTree::Node& node)
{
    return os << node.element;
}

void BinarySearchTree::insert_non_empty(BinarySearchTree::Node& node,
                                        double element)
{
    if (element < node.get_element()) {
        if (!node.has_left_child()) {
            node.create_left_child(element);
        } else {
            insert_non_empty(node.get_left_child(), element);
        }
    } else if (element > node.get_element()) {
        if (!node.has_right_child()) {
            node.create_right_child(element);
        } else {
            insert_non_empty(node.get_right_child(), element);
        }
    } else {
        throw std::runtime_error("Element already exists!");
    }

    node.update_height();
}
